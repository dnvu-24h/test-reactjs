import { createSlice } from '@reduxjs/toolkit';

const savedBrands = JSON.parse(localStorage.getItem('brands')) || [];

export const brandSlice = createSlice({
  name: 'brand',
  initialState: {
    brands: savedBrands,
  },
  reducers: {
    add: (state, action) => {
      const newBrands = state.brands;
      newBrands.push(action.payload);
      state.brands = newBrands;
      localStorage.setItem('brands', JSON.stringify(newBrands));
    },
    deleteBrand: (state, action) => {
      let newBrands = state.brands;
      const index = newBrands.findIndex(brand => brand.id === action.payload);
      if (index >= 0) {
        newBrands = [
          ...newBrands.slice(0, index),
          ...newBrands.slice(index + 1, newBrands.length)
        ]
      }
      state.brands = newBrands;
      localStorage.setItem('brands', JSON.stringify(newBrands));
    }
  },
});

export const { add, deleteBrand } = brandSlice.actions;

export const brands = state => state.brand.brands;

export default brandSlice.reducer;
