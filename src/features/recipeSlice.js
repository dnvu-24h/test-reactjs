import { createSlice } from '@reduxjs/toolkit';

const savedRecipes = JSON.parse(localStorage.getItem('recipes')) || [];

export const recipeSlice = createSlice({
  name: 'recipe',
  initialState: {
    recipes: savedRecipes,
  },
  reducers: {
    add: (state, action) => {
      const newRecipes = state.recipes;
      newRecipes.push(action.payload);
      state.recipes = newRecipes;
      localStorage.setItem('recipes', JSON.stringify(newRecipes));
    },
    addToFavorites: (state, action) => {
      const newRecipes = state.recipes;
      const index = newRecipes.findIndex(recipe => recipe.id === action.payload);
      if (index >= 0) {
        newRecipes[index] = {
          ...newRecipes[index],
          isFavorite: true
        }
      }
      state.recipes = newRecipes;
      localStorage.setItem('recipes', JSON.stringify(newRecipes));
    },
    deleteRecipe: (state, action) => {
      let newRecipes = state.recipes;
      const index = newRecipes.findIndex(recipe => recipe.id === action.payload);
      if (index >= 0) {
        newRecipes = [
          ...newRecipes.slice(0, index),
          ...newRecipes.slice(index + 1, newRecipes.length)
        ]
      }
      state.recipes = newRecipes;
      localStorage.setItem('recipes', JSON.stringify(newRecipes));
    }
  },
});

export const { add, addToFavorites, deleteRecipe } = recipeSlice.actions;

export const recipes = state => state.recipe.recipes;

export default recipeSlice.reducer;
