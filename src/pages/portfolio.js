import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { Button, Grid, Paper } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import ShowRecipe from '../components/showRecipe';
import ShowBrand from '../components/showBrand';
import { useSelector } from 'react-redux';
import { recipes } from "../features/recipeSlice";
import { brands } from "../features/brandSlice";

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  list: {
    margin: '20px 0'
  }
}));

const PortfolioPage = () => {
  const classes = useStyles();
  const history = useHistory();
  const arrRecipes = useSelector(recipes);
  const arrBrands = useSelector(brands);
  const [page, setPage] = useState(1);
  const perPage = 4;
  const totalPage = arrRecipes.length % perPage === 0 ? arrRecipes.length / perPage : parseInt(arrRecipes.length / perPage) + 1;
  console.log({ totalPage });
  const handleChange = (_, value) => {
    setPage(value);
  };

  // arrRecipes.slice().sort((a, b) => {
  //   return (+b.isFavorite) - (+a.isFavorite);
  // });

  return (
    <div className={classes.root}>
      <div>
        <h1>Recipes</h1>
        <Button variant="contained" color="primary" onClick={() => history.push('/portfolio/add-recipe')}>
          Add recipe
        </Button>
        <div className={classes.list}>
          <Grid container spacing={3}>
            {arrRecipes
              .slice()
              .sort((a, b) => {
                return (+b.isFavorite) - (+a.isFavorite);
              })
              .slice((page - 1) * perPage, page * perPage)
              .map((recipe, index) => (
                <Grid item xs={12} sm={6} md={3}>
                  <Paper className={classes.paper}>
                    <ShowRecipe key={index} recipe={{ ...recipe }} />
                  </Paper>
                </Grid>
              ))
            }
          </Grid>
        </div>
        {arrRecipes.length > 0 && <Pagination count={totalPage} page={page} onChange={handleChange} />}
      </div>
      <div>
        <h1>Favorite brands</h1>
        <Button variant="contained" color="primary" onClick={() => history.push('/portfolio/add-favorite-brand')}>
          Add favorite brand
        </Button>
        <div className={classes.list}>
          <Grid container spacing={3}>
            {arrBrands
              .map((brand, index) => (
                <Grid item xs={12} sm={6} md={3}>
                  <Paper className={classes.paper}>
                    <ShowBrand key={index} brand={{ ...brand }} />
                  </Paper>
                </Grid>
              ))
            }
          </Grid>
        </div>
      </div>
    </div>
  )
}

export default PortfolioPage;