import React from 'react';
import AddBrand from '../components/addBrand';

const AddBrandPage = () => {
  return (
    <div className='add-brand'>
      <AddBrand />
    </div>
  )
}

export default AddBrandPage;