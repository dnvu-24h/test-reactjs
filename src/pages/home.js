import React from 'react';
import { NavLink } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3),
    '& > *': {
      margin: theme.spacing(1),
    },
  }
}));

const HomePage = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <h1>Easier Chef</h1>
      <NavLink to="/portfolio">Portfolio</NavLink>
    </div>
  )
}

export default HomePage;