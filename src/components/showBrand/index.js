import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardContent, IconButton, Typography } from '@material-ui/core';
import { Delete as DeleteIcon } from '@material-ui/icons';
import { useDispatch } from 'react-redux';
import { deleteBrand } from '../../features/brandSlice';

const useStyles = makeStyles((theme) => ({
  root: {
    // width: 345,
    // margin: 10,
    '& .MuiCardContent-root': {
      display: 'flex !important',
      padding: '0 20px',
      alignItems: 'center',
      justifyContent: 'space-between',
    }
  }
}));

const ShowBrand = ({ brand }) => {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <Card className={classes.root}>
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          {brand.name}
        </Typography>
        <IconButton aria-label="delete" onClick={() => dispatch(deleteBrand(brand.id))}>
          <DeleteIcon />
        </IconButton>
      </CardContent>
    </Card>
  )
}

export default ShowBrand;