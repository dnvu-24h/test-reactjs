import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { add } from '../../features/brandSlice';
import { v4 as uuidv4 } from 'uuid';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3)
  },
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  field: {
    margin: theme.spacing(1),
    width: 600,
  },
  layoutBottom: {
    display: 'flex'
  },
  group: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center'
  }
}));

const AddBrand = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [brand, setBrand] = useState({
    id: uuidv4(),
    name: ''
  });

  return (
    <div className={classes.root}>
      <h1 className={classes.title}>Add favorite brand</h1>
      <form className={classes.layout} noValidate autoComplete="off">
        <TextField
          className={classes.field}
          id=""
          label="Name of the brand"
          variant="outlined"
          onChange={(e) => setBrand({
            ...brand,
            name: e.target.value
          })}
        />
        <Button variant="contained" color="primary" onClick={() => {
          dispatch(add(brand));
          history.push('/portfolio');
        }}>Add</Button>
      </form>
    </div>
  );
}

export default AddBrand;