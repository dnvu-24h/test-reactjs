import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Card, CardHeader, CardMedia, CardContent, CardActions, IconButton, Typography } from '@material-ui/core';
import { Favorite as FavoriteIcon, Delete as DeleteIcon } from '@material-ui/icons';
import { useDispatch } from 'react-redux';
import { addToFavorites, deleteRecipe } from '../../features/recipeSlice';

const useStyles = makeStyles((theme) => ({
  root: {
    // width: 345,
    // margin: 10
  },
  media: {
    height: 0,
    paddingTop: '56.25%', // 16:9
  },
  icon: {
    color: '#de1f1f'
  }
}));

export default function ShowRecipe({ recipe }) {
  const classes = useStyles();
  const dispatch = useDispatch();

  return (
    <Card className={classes.root}>
      <CardHeader
        title={recipe.dishName}
      />
      <CardMedia
        className={classes.media}
        image={recipe.dishImage}
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
          Time it takes to prepare the dish: {recipe.time}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          No of calories: {recipe.calories}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          Cooking difficulty: {recipe.cookingDifficulty}
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
        <IconButton
          className={recipe.isFavorite && classes.icon}
          aria-label="add to favorites"
          onClick={() => dispatch(addToFavorites(recipe.id))}
        >
          <FavoriteIcon />
        </IconButton>
        <IconButton aria-label="delete" onClick={() => dispatch(deleteRecipe(recipe.id))}>
          <DeleteIcon />
        </IconButton>
      </CardActions>
    </Card>
  );
}