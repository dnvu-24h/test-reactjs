import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { makeStyles } from '@material-ui/core/styles';
import { TextField, Button, InputLabel, MenuItem, FormControl, Select } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { add } from '../../features/recipeSlice';
import { v4 as uuidv4 } from 'uuid';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: theme.spacing(3)
  },
  layout: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  field: {
    margin: theme.spacing(1),
    width: 600,
  },
  layoutBottom: {
    display: 'flex'
  },
  group: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  title: {
    textAlign: 'center'
  }
}));

const AddRecipe = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const history = useHistory();
  const [recipe, setRecipe] = useState({
    id: uuidv4(),
    dishName: '',
    dishImage: '',
    time: '',
    calories: '',
    cookingDifficulty: '',
    isFavorite: false
  });

  return (
    <div className={classes.root}>
      <h1 className={classes.title}>Add recipe</h1>
      <form className={classes.layout} noValidate autoComplete="off">
        <TextField
          className={classes.field}
          id=""
          label="Name of the dish"
          variant="outlined"
          onChange={(e) => setRecipe({
            ...recipe,
            dishName: e.target.value
          })}
        />
        <TextField
          className={classes.field}
          id=""
          label="Image of the dish"
          variant="outlined"
          onChange={(e) => setRecipe({
            ...recipe,
            dishImage: e.target.value
          })}
        />
        <TextField
          className={classes.field}
          id=""
          type="number"
          label="Time it takes to prepare the dish"
          variant="outlined"
          onChange={(e) => setRecipe({
            ...recipe,
            time: e.target.value
          })}
        />
        <TextField
          className={classes.field}
          id=""
          type="number"
          label="No of calories"
          variant="outlined"
          onChange={(e) => setRecipe({
            ...recipe,
            calories: e.target.value
          })}
        />
        <FormControl variant="outlined" className={classes.field}>
          <InputLabel id="demo-simple-select-outlined-label">Cooking difficulty</InputLabel>
          <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            value={recipe.cookingDifficulty}
            onChange={(e) => setRecipe({
              ...recipe,
              cookingDifficulty: e.target.value
            })}
            label="Cooking difficulty"
          >
            <MenuItem value="easy">Easy</MenuItem>
            <MenuItem value="medium">Medium</MenuItem>
            <MenuItem value="hard">Hard</MenuItem>
          </Select>
        </FormControl>
        <Button variant="contained" color="primary" onClick={() => {
          dispatch(add(recipe));
          history.push('/portfolio');
        }}>Add</Button>
      </form>
    </div>
  );
}

export default AddRecipe;