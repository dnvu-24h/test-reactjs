import { configureStore } from '@reduxjs/toolkit';
import recipeReducer from '../features/recipeSlice';
import brandReducer from '../features/brandSlice';

export default configureStore({
  reducer: {
    recipe: recipeReducer,
    brand: brandReducer
  },
});
