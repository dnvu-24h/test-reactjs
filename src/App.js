import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import HomePage from './pages/home';
import PortfolioPage from './pages/portfolio';
import AddRecipePage from './pages/addRecipe';
import AddBrandPage from './pages/addBrand';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/" exact component={HomePage} />
          <Route path="/portfolio" exact component={PortfolioPage} />
          <Route path="/portfolio/add-recipe" exact component={AddRecipePage} />
          <Route path="/portfolio/add-favorite-brand" exact component={AddBrandPage} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
